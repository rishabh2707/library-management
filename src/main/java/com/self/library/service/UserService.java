package com.self.library.service;

import com.self.library.entity.User;
import com.self.library.repository.UserRepository;
import com.self.library.request.UserAdd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.soap.SOAPBinding;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User storeUserInfo(UserAdd userAdd)
    {
        return userRepository.save(createUserEntity(userAdd));
    }

    public List<User> findAllUser()
    {
        return userRepository.findAll();
    }

    public Optional<User> findUserById(Long id)
    {
        return userRepository.findById(id);
    }



    private User createUserEntity(UserAdd userAdd)
    {
        User user = new User();
        user.setFirstName(userAdd.getFirstName());
        user.setLastName(userAdd.getLastName());
        return user;
    }
}
