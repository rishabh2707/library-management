package com.self.library.controller;

import com.self.library.entity.User;
import com.self.library.request.UserAdd;
import com.self.library.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/")
    public User storeUserDetails(@Valid @RequestBody UserAdd userAdd)
    {
        return userService.storeUserInfo(userAdd);
    }

    @GetMapping("/")
    public List<User> getAllUserDetails()
    {
        return userService.findAllUser();
    }
}
