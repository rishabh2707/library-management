package com.self.library;


import org.hibernate.ejb.HibernatePersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.sql.DataSource;
import java.util.Properties;

@PropertySource("classpath:database.properties")
@Configuration
@EnableJpaRepositories("com.self.library.repository")
public class DataConfig {

    private final String PROPERTY_DRIVER = "driver";
    private final String PROPERTY_URL = "url";
    private final String PROPERTY_USERNAME = "user";
    private final String PROPERTY_PASSWORD = "password";
    private final String PROPERTY_SHOW_SQL = "hibernate.show_sql";
    private final String PROPERTY_DIALECT = "hibernate.dialect";

    @Autowired
    Environment env;


    LocalContainerEntityManagerFactoryBean entityManagerFactory()
    {
        LocalContainerEntityManagerFactoryBean lb = new LocalContainerEntityManagerFactoryBean();
        lb.setDataSource(dataSource());
        lb.setPersistenceProviderClass(HibernatePersistence.class);
        lb.setPackagesToScan("com.self.library.entity");
        lb.setJpaProperties(hibernateProps());
        return lb;
    }

    @Bean
    DataSource dataSource()
    {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName(env.getProperty(PROPERTY_DRIVER));
        ds.setUsername(env.getProperty(PROPERTY_USERNAME));
        ds.setUrl(env.getProperty(PROPERTY_URL));
        ds.setPassword(env.getProperty(PROPERTY_PASSWORD));
        return ds;
    }


    Properties hibernateProps()
    {
        Properties prop = new Properties();
        prop.setProperty(PROPERTY_DIALECT,env.getProperty(PROPERTY_DIALECT));
        prop.setProperty(PROPERTY_SHOW_SQL,env.getProperty(PROPERTY_SHOW_SQL));
        return prop;
    }

    /*@Bean
    JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }*/
}
